/* relish: versatile lisp shell
 * Copyright (C) 2021 Aidan Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::eval::eval;
use crate::segment::{Ctr, Seg, Type};
use crate::sym::{Args, Symbol, UserFn, ValueType, SYM_TABLE};
use std::env;

/*
// the stdlib var export function with env_sync on
static LIB_STORE_ENV: Symbol = Symbol {
    name: String::from("export"),
    args: Args::Lazy(2),
    value: ValueType::Internal(Box::new( |ast: &Seg| -> Ctr {
        _store_callback(ast, true)
    },
    )),
    has_undefined_symbols: false,
};

// the stdlib var export function with env_sync off
pub static LIB_STORE_NO_ENV: Symbol = Symbol {
    name: String::from("export"),
    args: Args::Lazy(2),
    value: ValueType::Internal(Box::new( |ast: &Seg| -> Ctr {
        _store_callback(ast, false)
    },
    )),
    has_undefined_symbols: false,
};*/

// TODO : declare function if arg list is long enough
fn _store_callback(ast: &Seg, env_cfg: bool) -> Ctr {
    let mut table_handle = SYM_TABLE.lock().unwrap();
    let is_var = ast.len() == 2;
    if let Ctr::Symbol(ref identifier) = *ast.car {
        match &*ast.cdr {
            Ctr::Seg(data_tree) if is_var => match eval(&Box::new(data_tree), true, true) {
                Ok(seg) => {
                    if let Ctr::Seg(ref val) = *seg {
                        table_handle.insert(
                            identifier.clone(),
                            Symbol {
                                value: ValueType::VarForm(val.car.clone()),
                                name: identifier.clone(),
                                args: Args::None,
                                has_undefined_symbols: false,
                            },
                        );
                        if env_cfg {
                            env::set_var(identifier.clone(), val.car.to_string());
                        }
                    } else {
                        eprintln!("impossible args to export")
                    }
                }
                Err(e) => eprintln!("couldnt eval symbol: {}", e),
            },
            Ctr::Seg(data_tree) if !is_var => {
                if let Ctr::Seg(ref args) = *data_tree.car {
                    let mut arg_list = vec![];
                    if !args.circuit(&mut |c: &Ctr| -> bool {
                        if let Ctr::Symbol(ref arg) = c {
                            arg_list.push(arg.clone());
                            true
                        } else {
                            false
                        }
                    }) {
                        eprintln!("all arguments defined for function must be of type symbol");
                        return Ctr::None;
                    };

                    if let Ctr::Seg(ref bodies) = *data_tree.cdr {
                        table_handle.insert(
                            identifier.clone(),
                            Symbol {
                                value: ValueType::FuncForm(UserFn {
                                    ast: Box::new(bodies.clone()),
                                    arg_syms: arg_list.clone(),
                                }),
                                name: identifier.clone(),
                                args: Args::Strict(arg_list.into_iter().map(Type::from).collect()),
                                has_undefined_symbols: false,
                            },
                        );
                    } else {
                        eprintln!("expected one or more function bodies in function definition");
                        return Ctr::None;
                    }
                } else {
                    eprintln!("expected list of arguments in function definition");
                    return Ctr::None;
                }
            }
            Ctr::None => {
                table_handle.remove(&identifier.to_string());
                if env_cfg {
                    env::remove_var(identifier);
                }
            }
            _ => eprintln!("args not in standard form"),
        }
    } else {
        eprintln!("first argument to export must be a symbol");
    }
    Ctr::None
}
