/* relish: versatile lisp shell
 * Copyright (C) 2021 Aidan Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::segment::{Ctr, Seg};
use crate::sym::{Symbol, ValueType, SYM_TABLE};

/// This function iterates over a syntax tree, returning a new list of values
/// representing the simplest possible form of the input
///
/// # Example
/// ```
/// // TODO
/// ```
/// # Errors
/// TODO
///
/// # Panics
/// Panics if the symbol table is already locked. I.e., the calling code should
/// NOT hold a lock on the current thread before invocation of this function!

// This is temporary to shut clippy up about long function. Remove this!
#[allow(clippy::too_many_lines)]

pub fn eval(
    ast: &Seg,
    expect_all_symbols_defined: bool,
    simplify_function_branches: bool,
) -> Result<Box<Ctr>, String> {
    // data to return
    let mut ret = Box::from(Ctr::None);
    let mut first = true;

    // to be assigned from cloned/evaled data
    let mut car;
    let mut cdr = Box::from(Ctr::None);

    // lets me redirect the input
    let mut arg_car = &ast.car;
    let mut arg_cdr = &ast.cdr;

    let table_handle = SYM_TABLE.lock().unwrap();

    // iterate over ast and build out ret
    loop {
        let mut prefetched_function: Option<&Symbol> = None;
        while let Ctr::Symbol(ref tok) = **arg_car {
            prefetched_function = table_handle.get(tok);
            if let Some(sym_ref) = prefetched_function {
                if let ValueType::VarForm(ref value) = sym_ref.value {
                    arg_car = value;
                } else {
                    break;
                }
            } else if !expect_all_symbols_defined {
                return Err(format!("evaluation error: undefined symbol {tok}"));
            } else {
                break;
            }
        }

        match **arg_car {
            Ctr::Seg(ref inner) => {
                car = eval(
                    inner,
                    expect_all_symbols_defined,
                    simplify_function_branches,
                )?;
            }

            // im tired please simplify this
            Ctr::Symbol(_) => {
                if simplify_function_branches && first {
                    if let Some(func) = prefetched_function {
                        if let Ctr::Seg(ref candidates) = **arg_cdr {
                            let fc: Result<Box<Ctr>, String>;
                            match eval(candidates, expect_all_symbols_defined, false) {
                                Ok(res) => {
                                    match *res {
                                        Ctr::Seg(ref args) => {
                                            fc = func.call(args);
                                        }
                                        _ => fc = func.call(&Seg::from_mono(res.clone())),
                                    }
                                    match fc {
                                        Ok(datum) => car = datum,
                                        Err(e) => {
                                            return Err(format!(
                                                "call to {} failed: {}",
                                                func.name, e
                                            ))
                                        }
                                    }
                                }
                                Err(e) => return Err(format!("evaluation error: {e}")),
                            }
                        } else {
                            match func.call(&Seg::new()) {
                                Ok(res) => car = res,
                                Err(e) => {
                                    return Err(format!("call to {} failed: {}", func.name, e))
                                }
                            }
                        }
                    } else {
                        car = arg_car.clone();
                    }
                } else {
                    car = arg_car.clone();
                }
            }

            _ => {
                car = arg_car.clone();
            }
        }

        // weird tree but okay
        while let Ctr::Symbol(ref tok) = **arg_cdr {
            prefetched_function = table_handle.get(tok);
            if let Some(sym_ref) = prefetched_function {
                if let ValueType::VarForm(ref value) = sym_ref.value {
                    arg_cdr = value;
                } else {
                    break;
                }
            } else if !expect_all_symbols_defined {
                return Err(format!("evaluation error: undefined symbol {tok}"));
            } else {
                break;
            }
        }

        match **arg_cdr {
            Ctr::Symbol(_) => {
                cdr = arg_cdr.clone();
                break;
            }

            Ctr::Seg(ref next) => {
                if let Ctr::None = *ret {
                    *ret = Ctr::Seg(Seg::from(car, cdr.clone()));
                } else if let Ctr::Seg(ref mut s) = *ret {
                    s.append(Box::from(Ctr::Seg(Seg::from(car, cdr.clone()))));
                }
                arg_car = &next.car;
                arg_cdr = &next.cdr;
            }

            _ => {
                cdr = ast.cdr.clone();
                break;
            }
        }

        if let Ctr::None = **arg_car {
            if let Ctr::None = **arg_cdr {
                break;
            }
        }

        first = false;
    }

    Ok(ret)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::{lex, Args, Ctr, Seg, Symbol, UserFn, ValueType, SYM_TABLE};

    #[test]
    fn singlet() {
        let test_doc = "(1)";
        let initial_ast = lex(test_doc).unwrap();
        let reduced = eval(&initial_ast, true, true).unwrap().to_string();
        assert_eq!(reduced, test_doc);
    }

    #[test]
    fn embedded_lists_no_funcs() {
        let test_doc = "(1 (1 2 3 4 5) 5)";
        let initial_ast = lex(test_doc).unwrap();
        let reduced = eval(&initial_ast, true, true).unwrap().to_string();
        assert_eq!(reduced, test_doc);
    }

    #[test]
    fn function_call() {
        let test_external_func = Symbol {
            name: "echo".into(),
            args: Args::Lazy(1),
            has_undefined_symbols: false,
            value: ValueType::FuncForm(UserFn {
                arg_syms: vec!["input".to_string()],
                ast: Box::new(Seg::from(
                    Box::new(Ctr::Seg(Seg::from(
                        Box::from(Ctr::Symbol("input".to_string())),
                        Box::from(Ctr::None),
                    ))),
                    Box::new(Ctr::None),
                )),
            }),
        };

        SYM_TABLE
            .lock()
            .unwrap()
            .insert("echo".into(), test_external_func);

        let initial_ast = lex("('one' (echo 'unwrap_me))").unwrap();
        let reduced = eval(&initial_ast, true, true).unwrap();

        assert_eq!(reduced.to_string(), "('one' 'unwrap_me')");
    }

    #[test]
    fn embedded_func_calls() {
        let test_external_func = Symbol {
            name: "echo".into(),
            args: Args::Lazy(1),
            has_undefined_symbols: false,
            value: ValueType::FuncForm(UserFn {
                arg_syms: vec!["input".to_string()],
                ast: Box::new(Seg::from(
                    Box::new(Ctr::Seg(Seg::from(
                        Box::from(Ctr::Symbol("input".to_string())),
                        Box::from(Ctr::None),
                    ))),
                    Box::new(Ctr::None),
                )),
            }),
        };

        SYM_TABLE
            .lock()
            .unwrap()
            .insert("echo".into(), test_external_func);

        let initial_ast = lex("('one' (echo (echo 'unwrap_me')))").unwrap();

        let reduced = eval(&initial_ast, true, true).unwrap();
        assert_eq!(reduced.to_string(), "('one' 'unwrap_me')");
    }
}
