/* relish: highly versatile lisp interpreter
 * Copyright (C) 2021 Aidan Hahn
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::segment::{Ctr, Seg};

#[derive(Debug, PartialEq, Clone)]
pub enum LexingError {
    NonAsciiCharacters,
    EmptyDocument,
    TooManyEndParans,
    ListStartedInMiddleOfToken,
    EmptyToken,
    Unparsable(String),
    UnmatchedStringDelimiter,
    UnmatchedListDelimiter,
}

impl std::fmt::Display for LexingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LexingError::NonAsciiCharacters => {
                write!(f, "Document may only contain ASCII characters")
            }
            LexingError::EmptyDocument => write!(f, "Empty Document"),
            LexingError::TooManyEndParans => write!(f, "Too many end parenthesis"),
            LexingError::ListStartedInMiddleOfToken => {
                write!(f, "New list started in middle of token")
            }
            LexingError::EmptyToken => write!(f, "Empty token"),
            LexingError::Unparsable(token) => write!(f, "Cannot parse token: {token}"),
            LexingError::UnmatchedStringDelimiter => {
                write!(f, "Unmatched string delimiter in input")
            }
            LexingError::UnmatchedListDelimiter => write!(f, "Unmatched list delimiter in input"),
        }
    }
}

impl std::error::Error for LexingError {}

/* takes a line of user input
 * returns an unsimplified tree of tokens.
 */
pub fn lex(document: &str) -> Result<Box<Seg>, LexingError> {
    if !document.is_ascii() {
        return Err(LexingError::NonAsciiCharacters);
    }

    let tree = process(document)?;
    // TODO: Make multiple forms of Ok()
    // To represent the multiple passable outcomes
    Ok(tree)
}

/* The logic used in lex
 * Returns Ok(Rc<Seg>) if lexing passes
 * Returns Err(String) if an error occurs
 */
fn process(document: &str) -> Result<Box<Seg>, LexingError> {
    let doc_len = document.len();

    if doc_len == 0 {
        return Err(LexingError::EmptyDocument);
    }

    /* State variables
     * TODO: describe all of them
     */
    let mut is_str = false;
    let mut ign = false;
    let mut token = String::new();
    let mut delim_stack = Vec::new();
    let mut ref_stack = vec![];

    /* Iterate over document
     * Manage currently sought delimiter
     */
    for c in document.chars() {
        let mut needs_alloc = false;
        let mut alloc_list = false;
        let delim;
        if let Some(d) = delim_stack.last() {
            delim = *d;

            if delim == '*' {
                token.push(c);
                delim_stack.pop();
                continue;

            // normal delimiter cases
            } else if c == delim {
                needs_alloc = true;
                // reset comment line status
                if delim == '\n' {
                    delim_stack.pop();
                    ign = false;
                    continue;
                }

                // catch too many list end
                // set alloc_list
                if delim == ')' {
                    alloc_list = true;
                    if ref_stack.is_empty() {
                        return Err(LexingError::TooManyEndParans);
                    }
                }
                delim_stack.pop();

            // if we are in a commented out space, skip this char
            } else if ign {
                continue;
            }
        }
        // try to generalize all whitespace
        if !needs_alloc && c.is_whitespace() && !is_str {
            // dont make empty tokens just because the document has consecutive whitespace
            if token.is_empty() {
                continue;
            }
            needs_alloc = true;
        }
        // match a delimiter
        if !needs_alloc {
            match c {
                // add a new Seg reference to the stack
                '(' => {
                    if is_str {
                        token.push(c);
                        continue;
                    }

                    if !token.is_empty() {
                        return Err(LexingError::ListStartedInMiddleOfToken);
                    }

                    ref_stack.push(Seg::new());
                    delim_stack.push(')');
                }
                // begin parsing a string
                '"' | '\'' | '`' => {
                    is_str = true;
                    delim_stack.push(c);
                }
                // eat the whole line
                '#' => {
                    ign = true;
                    delim_stack.push('\n');
                }
                // escape next char
                '\\' => {
                    delim_stack.push('*');
                }
                // add to token
                _ => {
                    token.push(c);
                }
            }

        /* 1. Handle allocation of new Ctr
         * 2. Handle expansion of current list ref
         */
        } else {
            if token.is_empty() && !is_str && !alloc_list {
                return Err(LexingError::EmptyToken);
            }

            let mut current_seg = ref_stack.pop().unwrap();
            let obj;
            if is_str {
                obj = Box::from(Ctr::String(token));
                is_str = false;
                token = String::new();
                current_seg.append(obj);
            } else if !token.is_empty() {
                if token == "true" {
                    obj = Box::from(Ctr::Bool(true));
                } else if token == "false" {
                    obj = Box::from(Ctr::Bool(false));
                } else if let Ok(i) = token.parse::<i128>() {
                    obj = Box::from(Ctr::Integer(i));
                } else if let Ok(f) = token.parse::<f64>() {
                    obj = Box::from(Ctr::Float(f));
                } else if let Some(s) = tok_is_symbol(&token) {
                    obj = Box::from(Ctr::Symbol(s));
                } else {
                    return Err(LexingError::Unparsable(token));
                }

                token = String::new();
                current_seg.append(obj.clone());
            }

            if alloc_list {
                // return if we have finished the document
                if ref_stack.is_empty() {
                    return Ok(Box::new(current_seg));
                }

                let t = current_seg;
                current_seg = ref_stack.pop().unwrap();
                /* TODO: is there a way to do this that doesnt
                 * involve needlessly copying heap data? I am
                 * not sure what optimizations rustc performs
                 * but I assume this should not end up copying
                 * contained segments around.
                 */
                current_seg.append(Box::from(Ctr::Seg(t)));
            }

            ref_stack.push(current_seg);
        }
    }

    if is_str {
        Err(LexingError::UnmatchedStringDelimiter)
    } else {
        Err(LexingError::UnmatchedListDelimiter)
    }
}

/* Returns true if token
 *   - is all alphanumeric except dash and underscore
 *
 * else returns false
 */
fn tok_is_symbol(token: &str) -> Option<String> {
    for t in token.chars() {
        if !t.is_alphanumeric() && t != '-' && t != '_' {
            return None;
        }
    }

    Some(String::from(token))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_pair() {
        let document = "(hello 'world')";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), document);
    }

    #[test]
    fn basic_list() {
        let document = "(hello 'world' 1 2 3)";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), document);
    }

    #[test]
    fn complex_list() {
        let document = "(hello 'world' (1 2 (1 2 3)) 1 2 3)";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), document);
    }

    #[test]
    fn bad_symbol() {
        let document = "(as;dd)";
        let error = lex(document).expect_err("Token lexing failure");
        assert_eq!(error, LexingError::Unparsable("as;dd".into()))
    }

    #[test]
    fn list_deliminator_in_string() {
        let document = "('(')";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), document);
    }

    #[test]
    fn empty_string() {
        let document = "('')";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), document);
    }

    #[test]
    fn unmatched_list_delimiter_flat() {
        let document = "(one two";
        let error = lex(document).expect_err("Token lexing failure");
        assert_eq!(error, LexingError::UnmatchedListDelimiter)
    }

    #[test]
    fn unmatched_list_delimiter_complex() {
        let document = "(one two (three)";
        let error = lex(document).expect_err("Token lexing failure");
        assert_eq!(error, LexingError::UnmatchedListDelimiter)
    }

    #[test]
    fn comment() {
        let document = r"#!/bin/relish
(one two)";
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), "(one two)");
    }

    #[test]
    fn postline_comment() {
        let document = r#"#!/bin/relish
((one two)# another doc comment
(three four))"#;
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), "((one two) (three four))");
    }

    #[test]
    fn inline_comment() {
        let document = r#"#!/bin/relish
((one two)
# another doc comment
three)"#;
        let tree = lex(document).unwrap();
        assert_eq!(tree.to_string(), "((one two) three)");
    }

    #[test]
    fn bad_token_list() {
        let document = "(one t(wo)";
        let error = lex(document).expect_err("Token lexing failure");
        assert_eq!(error, LexingError::ListStartedInMiddleOfToken);
    }
}
