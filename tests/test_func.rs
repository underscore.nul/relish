mod func_tests {
    use relish::ast::lex;
    use relish::ast::{Args, Ctr, Seg, Symbol, ValueType};
    use relish::ast::{Type, UserFn, SYM_TABLE};

    #[test]
    fn decl_and_call_internal_func() {
        let mut table_handle = SYM_TABLE.lock().unwrap();
        let test_internal_func: Symbol = Symbol {
            name: "test_func_in".into(),
            has_undefined_symbols: false,
            args: Args::Strict(vec![Type::Bool]),
            value: ValueType::Internal(Box::new(|a: &Seg| -> Ctr {
                let inner = a;
                let mut is_bool = false;
                if let Ctr::Bool(_) = *inner.car {
                    is_bool = true;
                }

                Ctr::Bool(is_bool)
            })),
        };
        let args = Seg::from(Box::new(Ctr::Bool(true)), Box::new(Ctr::None));

        table_handle.insert("test_func_in".into(), test_internal_func);
        let func: &Symbol;
        if let Some(f) = table_handle.get(&"test_func_in".to_string()) {
            func = f;
        } else {
            panic!("failed to retrieve function!")
        }

        if let Ok(ret) = func.call(&args) {
            match *ret {
                Ctr::Bool(b) => assert!(b),
                _ => {
                    panic!("invalid return from func!")
                }
            }
        } else {
            panic!("call to function failed!")
        }
    }

    #[test]
    fn decl_and_call_external_func_singlet() {
        let mut table_handle = SYM_TABLE.lock().unwrap();
        match lex("((input))") {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let test_external_func: Symbol = Symbol {
                    name: "echo".into(),
                    has_undefined_symbols: false,
                    args: Args::Lazy(1),
                    value: ValueType::FuncForm(UserFn {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };

                let args = Seg::from(
                    Box::new(Ctr::String("test".to_string())),
                    Box::new(Ctr::None),
                );

                table_handle.insert("test_func_in".into(), test_external_func);
                let func: &Symbol;
                if let Some(f) = table_handle.get(&"test_func_in".to_string()) {
                    func = f;
                } else {
                    panic!("failed to retrieve function!")
                }

                match func.call(&args) {
                    Ok(ret) => match *ret {
                        Ctr::String(b) => assert!(b == "test"),
                        _ => {
                            panic!("Invalid return from func. Got {:?}\n", ret)
                        }
                    },
                    Err(e) => {
                        panic!("Call to function failed: {}\n", e)
                    }
                }
            }
        }
    }

    #[test]
    fn decl_and_call_external_func_multi_body() {
        let mut table_handle = SYM_TABLE.lock().unwrap();
        match lex("((input) (input))") {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let test_external_func: Symbol = Symbol {
                    name: "echo_2".into(),
                    has_undefined_symbols: false,
                    args: Args::Lazy(1),
                    value: ValueType::FuncForm(UserFn {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };

                let args = Seg::from(
                    Box::new(Ctr::String("test".to_string())),
                    Box::new(Ctr::None),
                );

                table_handle.insert("echo_2".into(), test_external_func);
                let func: &Symbol;
                if let Some(f) = table_handle.get(&"echo_2".to_string()) {
                    func = f;
                } else {
                    panic!("failed to retrieve function!")
                }

                match func.call(&args) {
                    Ok(ret) => assert_eq!(ret.to_string(), "(\"test\" \"test\")"),
                    Err(e) => {
                        panic!("Call to function failed: {}\n", e)
                    }
                }
            }
        }
    }

    #[test]
    fn decl_and_call_func_with_nested_call() {
        let mut table_handle = SYM_TABLE.lock().unwrap();
        let inner_func: Symbol = Symbol {
            name: "test_inner".into(),
            has_undefined_symbols: false,
            args: Args::Strict(vec![Type::Bool]),
            value: ValueType::Internal(Box::new(|a: &Seg| -> Ctr {
                let inner = a;
                if let Ctr::Bool(b) = *inner.car {
                    if b {
                        Ctr::String("test".to_string())
                    } else {
                        Ctr::None
                    }
                } else {
                    Ctr::None
                }
            })),
        };

        match lex("((test_inner true))") {
            Err(e) => panic!("{}", e),
            Ok(finner) => {
                let outer_func: Symbol = Symbol {
                    name: "test_outer".into(),
                    has_undefined_symbols: false,
                    args: Args::Lazy(1),
                    value: ValueType::FuncForm(UserFn {
                        arg_syms: vec!["input".to_string()],
                        ast: finner,
                    }),
                };

                let args = Seg::from(Box::new(Ctr::Bool(true)), Box::new(Ctr::None));

                table_handle.insert("test_inner".into(), inner_func);
                table_handle.insert("test_outer".into(), outer_func);

                let func: &Symbol;
                if let Some(f) = table_handle.get(&"test_outer".to_string()) {
                    func = f;
                } else {
                    panic!("failed to retrieve function!")
                }

                match func.call(&args) {
                    Ok(ret) => match *ret {
                        Ctr::String(b) => assert!(b == "test"),
                        _ => {
                            panic!("Invalid return from func. Got {:?}\n", ret)
                        }
                    },
                    Err(e) => {
                        panic!("Call to function failed: {}\n", e)
                    }
                }
            }
        }
    }

    /*
    // TODO: These tests need completion!
    #[test]
    fn eval_lazy_func_call() {

    }

    #[test]
    fn sym_loose_func_call() {

    }

    #[test]
    fn too_many_args() {

    }

    #[test]
    fn not_enough_args() {

    }

    #[test]
    fn bad_eval_arg() {

    }

    #[test]
    fn bad_eval_fn_body() {

    }*/
}
