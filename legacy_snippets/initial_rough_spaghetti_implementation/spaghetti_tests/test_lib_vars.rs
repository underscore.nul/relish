mod var_lib_tests {
    use relish::ast::{eval, lex, Ctr, FTable, VTable};
    use relish::stdlib::get_stdlib;
    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn test_variable_export_and_lookup() {
        let doc1 = "(export test 1)";
        let doc2 = "(concat test)";
        let result = "1";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false);
            }
        }

        match lex(doc1.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}", doc1, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}", doc2, s);
                    assert!(false);
                }

                Ok(ctr) => {
                    println!("{:#?}", vt);
                    match ctr {
                        Ctr::None => assert!(true),
                        _ => assert!(false),
                    }
                }
            },
        }

        match lex(doc2.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}", doc2, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}", doc2, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::String(s) => assert_eq!(s, result),
                    _ => assert!(false),
                },
            },
        }
    }
}
